<!doctype html>
<html lang="fr">
  <head>

   <!-- Title -->
   <title>Protège-moi sur Internet - Sources</title>
   <link rel="canonical" href="https://protege-moi-sur-internet.com/sources.php">

   <!-- Meta -->
   <meta name="author" content="Yann Jaulin">
   <meta name="keywords" content="Sources, Privacy Guides, Techlore">
   <meta name="description" content="Bienvenue sur la page des sources, proposée par Protège-moi sur Internet !">

   <?php
      require_once 'require/lien.php';
   ?>
  
  </head>
  <body>

  <!-- Navigation -->
  <?php
    require_once 'require/nav.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Alert -->
  <?php
    require_once 'require/alert.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Fil d'ariane -->
  <div class="container">
    <div class="row">
        <div class="col-md">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/">Accueil</a></li>
              <li class="breadcrumb-item" aria-current="page">Sources</li>
            </ol>
          </nav>
        </div>
    </div>
  </div>

  <!--Séparateur-->
  <div class="py-4"></div>

    <!--Catégorie-->
    <div class="container">
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/privacyguides.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Privacy Guides</h5>
                  <p class="card-text">Le site de référence en matière de confidentialité et de sécurité pour se protéger sur Internet !</p>
                  <a href="https://privacyguides.org/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/techlore.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Techlore</h5>
                  <p class="card-text">Un site qui regroupe plusieurs projets, communautés et contenus afin de diffuser les notions de vie privée et de sécurité auprès du grand public !</p>
                  <a href="https://techlore.tech/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
        </div>
    </div>

    <!-- Séparateur -->
    <div class="py-2"></div>

    <!-- Footer -->
    <?php
      require_once 'require/footer.php';
    ?>

  </body>
</html>