<!doctype html>
<html lang="fr">
  <head>

   <!-- Title -->
   <title>Protège-moi sur Internet - Outils</title>
   <link rel="canonical" href="https://protege-moi-sur-internet.com/outils.php">

   <!-- Meta -->
   <meta name="author" content="Yann Jaulin">
   <meta name="keywords" content="Outils, AlternativeTo, CHATONS, Exodus Privacy, Have I Been Pwned, JustDeleteMe, Open Observatory of Network Interference, Open Terms Archive">
   <meta name="description" content="Bienvenue sur la page des outils, proposée par Protège-moi sur Internet !">

   <?php
      require_once 'require/lien.php';
   ?>
  
  </head>
  <body>

  <!-- Navigation -->
  <?php
    require_once 'require/nav.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Alert -->
  <?php
    require_once 'require/alert.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Fil d'ariane -->
  <div class="container">
    <div class="row">
        <div class="col-md">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/">Accueil</a></li>
              <li class="breadcrumb-item" aria-current="page">Outils</li>
            </ol>
          </nav>
        </div>
    </div>
  </div>

  <!--Séparateur-->
  <div class="py-4"></div>

    <!--Catégorie-->
    <div class="container">
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/alternativeto.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">AlternativeTo</h5>
                  <p class="card-text">Un site pour trouver des alternatives aux logiciels et services les plus connus !</p>
                  <a href="https://alternativeto.net/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/chatons.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">CHATONS</h5>
                  <p class="card-text">Un collectif qui regroupe de petites structures proposant des services en ligne libres, éthiques, décentralisés et solidaires !</p>
                  <a href="https://chatons.org/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/exodusprivacy.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Exodus Privacy</h5>
                  <p class="card-text">Une plateforme d’audit de la vie privée des applications Android !</p>
                  <a href="https://exodus-privacy.eu.org/fr/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/haveibeenpwned.png" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Have I Been Pwned</h5>
                  <p class="card-text">Une plateforme qui vérifie si vos données personnelles ont été compromises à la suite d'une fuite !</p>
                  <a href="https://haveibeenpwned.com/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/justdeleteme.png" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">JustDeleteMe</h5>
                  <p class="card-text">Un répertoire de liens pour supprimer vos comptes des sites Internet !</p>
                  <a href="https://justdeleteme.xyz/fr" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/openobservatoryofnetworkinterference.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Open Observatory of Network Interference</h5>
                  <p class="card-text">Une base de données ouverte sur la censure d'Internet dans le monde !</p>
                  <a href="https://explorer.ooni.org/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/opentermsarchive.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Open Terms Archive</h5>
                  <p class="card-text">Un logiciel libre qui enregistre les évolutions des conditions générales des services numériques !</p>
                  <a href="https://www.opentermsarchive.org/fr" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
        </div>
    </div>

    <!-- Séparateur -->
    <div class="py-2"></div>

    <!-- Footer -->
    <?php
      require_once 'require/footer.php';
    ?>

  </body>
</html>