<div class="container">
        <div class="row">
            <div class="col-md">
                <div class="alert alert-success" role="alert">
                    <strong>09/05/2023 :</strong> Une nouvelle alternative a été ajoutée et il s'agit de Snowflake, une extension de navigateur web communautaire contrant la censure. Vous pouvez la consulter en allant sur Navigation > Extension. Bonne navigation !
                </div>
            </div>
        </div>
    </div>
