<!doctype html>
<html lang="fr">
  <head>

   <!-- Title -->
   <title>Protège-moi sur Internet - Associations</title>
   <link rel="canonical" href="https://protege-moi-sur-internet.com/associations.php">

   <!-- Meta -->
   <meta name="author" content="Yann Jaulin">
   <meta name="keywords" content="Associations, April, Framasoft, La Quadrature du Net">
   <meta name="description" content="Bienvenue sur la page des associations, proposée par Protège-moi sur Internet !">

   <?php
      require_once 'require/lien.php';
   ?>
  
  </head>
  <body>

  <!-- Navigation -->
  <?php
    require_once 'require/nav.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Alert -->
  <?php
    require_once 'require/alert.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Fil d'ariane -->
  <div class="container">
    <div class="row">
        <div class="col-md">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/">Accueil</a></li>
              <li class="breadcrumb-item" aria-current="page">Associations</li>
            </ol>
          </nav>
        </div>
    </div>
  </div>

  <!--Séparateur-->
  <div class="py-4"></div>

    <!--Catégorie-->
    <div class="container">
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/april.png" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">April</h5>
                  <p class="card-text">Une association qui promeut et défend le logiciel libre !</p>
                  <a href="https://www.april.org/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/framasoft.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Framasoft</h5>
                  <p class="card-text">Une association qui soutient des projets libres et contributifs !</p>
                  <a href="https://framasoft.org/fr/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/laquadraturedunet.png" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">La Quadrature du Net</h5>
                  <p class="card-text">Une association qui promeut et défend les droits et les libertés sur Internet !</p>
                  <a href="https://www.laquadrature.net/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
        </div>
    </div>

    <!-- Séparateur -->
    <div class="py-2"></div>

    <!-- Footer -->
    <?php
      require_once 'require/footer.php';
    ?>

  </body>
</html>