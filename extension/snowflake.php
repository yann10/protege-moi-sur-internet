<!doctype html>
<html lang="fr">
  <head>

   <!-- Title -->
   <title>Protège-moi sur Internet - Snowflake</title>
   <link rel="canonical" href="https://protege-moi-sur-internet.com/extension/snowflake.php">

   <!-- Meta -->
   <meta name="author" content="Yann Jaulin">
   <meta name="keywords" content="Navigation, Extension, Snowflake, The Tor Project, Inc">
   <meta name="description" content="Bienvenue sur la page de présentation de Snowflake, proposée par Protège-moi sur Internet !">

   <?php
      require_once '../require/lien.php';
   ?>
  
  </head>
  <body>

    <!-- Navigation -->
    <?php
      require_once '../require/nav.php';
    ?>

    <!--Séparateur-->
    <div class="py-4"></div>

    <!-- Alert -->
    <?php
      require_once '../require/alert.php';
    ?>

   <!--Séparateur-->
   <div class="py-4"></div>

   <!-- Fil d'ariane -->
  <div class="container">
    <div class="row">
        <div class="col-md">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/">Accueil</a></li>
                  <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/extension/">Extension</a></li>
                  <li class="breadcrumb-item" aria-current="page">Snowflake</li>
                </ol>
            </nav>
        </div>
    </div>
  </div>

  <!--Séparateur-->
  <div class="py-4"></div>

    <!--Présentation-->
    <div class="container">
        <div class="row">
            <div class="col-md">
                <!-- Logo -->
                <div class="text-center">
                    <figure>
                        <img src="https://protege-moi-sur-internet.com/images/snowflake.png" class="img-thumbnail" alt="Logo de Snowflake">
                        <figcaption>Logo du site de Snowflake</figcaption>
                    </figure>
                </div>

                <!-- Séparateur -->
                <div class="py-4"></div>

                <!-- Titre -->
                <h1 class="font-weight-bold text-center">Présentation de Snowflake</h1>

                <!-- Séparateur -->
                <div class="py-4"></div>

                <!-- Présentation -->
                <p class="lead text-center">Snowflake est une extension de navigateur web créée par la fondation américaine The Tor Project, Inc.</p>

                <!-- Séparateur -->
                <div class="py-2"></div>

                <!-- Présentation -->
                <p class="lead text-center">C'est un outil qui permet d'aider les habitants des pays censurés à accéder sur internet grâce à la participation de la communauté.</p>
                
                <!-- Séparateur -->
                <div class="py-2"></div>

                <!-- Présentation -->
                <p class="lead text-center">Pour cela, elle transforme votre navigateur en une passerelle d'accès au réseau chiffré Tor et les habitants ayant besoin utilisent le navigateur Tor qui choisit plusieurs passerelles pour pouvoir y accéder.</p>

                <!-- Séparateur -->
                <div class="py-2"></div>

                <!-- Présentation -->
                <p class="lead text-center">Open source et sécurisé, vous n'êtes jamais connu, car les sites visités par les utilisateurs sont liés à un nœud de sortie et non d'entrée, Snowflake créant uniquement un nœud d'entrée.</p>

                <!-- Séparateur -->
                <div class="py-2"></div>

                <!-- Présentation -->
                <p class="lead text-center">Enfin, il est aussi possible d'y participer sans installer l'extension en allant sur un site web proposant la technologie.</p>
                
                <!-- Séparateur -->
                <div class="py-4"></div>
                 
                <!-- Titre -->
                <h2 class="font-weight-bold text-center">Bonne utilisation !</h2>

                 <!-- Séparateur -->
                <div class="py-4"></div>

                <!-- site -->
                <div class="btn-toolbar justify-content-center" role="toolbar" aria-label="Groupe de liens">
                  <div class="btn-group mr-2" role="group" aria-label="Site de Snowflake">
                    <a href="https://snowflake.torproject.org/?lang=fr" target="_blank" role="button" class="btn">Site officiel <i class="fa fa-external-link" aria-hidden="true"></i></a>
                  </div>
                  <div class="btn-group mr-2" role="group" aria-label="Add-ons de Snowflake">
                    <a href="https://addons.mozilla.org/fr/firefox/addon/torproject-snowflake/" target="_blank" role="button" class="btn">Add-ons Firefox <i class="fa fa-external-link" aria-hidden="true"></i></a>
                  </div>
                  <div class="btn-group" role="group" aria-label="Passerelle Snowflake">
                    <a href="https://snowflake.torproject.org/embed.html" target="_blank" role="button" class="btn">Passerelle web <i class="fa fa-external-link" aria-hidden="true"></i></a>
                  </div>
                </div>

                <!-- Séparateur -->
                <div class="py-2"></div>
            </div>
        </div>
    </div>

    <!-- Séparateur -->
    <div class="py-2"></div>

    <!-- Footer -->
    <?php
      require_once '../require/footer.php';
    ?>

  </body>
</html>