<!doctype html>
<html lang="fr">
  <head>

   <!-- Title -->
   <title>Protège-moi sur Internet - OLinuXino</title>
   <link rel="canonical" href="https://protege-moi-sur-internet.com/materiels/olinuxino.php">

   <!-- Meta -->
   <meta name="author" content="Yann Jaulin">
   <meta name="keywords" content="Matériels, OLinuXino, Olimex Ltd, FreedomBox, YunoHost">
   <meta name="description" content="Bienvenue sur la page de présentation de OLinuXino, proposée par Protège-moi sur Internet !">

   <?php
      require_once '../require/lien.php';
   ?>

  </head>
  <body>

    <!-- Navigation -->
    <?php
      require_once '../require/nav.php';
    ?>

    <!--Séparateur-->
    <div class="py-4"></div>

    <!-- Alert -->
    <?php
      require_once '../require/alert.php';
    ?>

   <!--Séparateur-->
   <div class="py-4"></div>

   <!-- Fil d'ariane -->
  <div class="container">
    <div class="row">
        <div class="col-md">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/">Accueil</a></li>
                  <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/materiels/">Matériels</a></li>
                  <li class="breadcrumb-item" aria-current="page">OLinuXino</li>
                </ol>
            </nav>
        </div>
    </div>
  </div>

  <!--Séparateur-->
  <div class="py-4"></div>

    <!--Présentation-->
    <div class="container">
        <div class="row">
            <div class="col-md">
                <!-- Logo -->
                <div class="text-center">
                    <figure>
                        <img src="https://protege-moi-sur-internet.com/images/olinuxino.jpg" class="img-thumbnail" alt="Logo de OLinuXino">
                        <figcaption>Logo du site de OLinuXino</figcaption>
                    </figure>
                </div>

                <!-- Séparateur -->
                <div class="py-4"></div>

                <!-- Titre -->
                <h1 class="font-weight-bold text-center">Présentation de OLinuXino</h1>

                <!-- Séparateur -->
                <div class="py-4"></div>

                <!-- Présentation -->
                <p class="lead text-center">OLinuXino est un nano-ordinateur de la société bulgare Olimex Ltd.</p>

                <!-- Séparateur -->
                <div class="py-2"></div>

                <!-- Présentation -->
                <p class="lead text-center">Open hardware et open source, il est construit sur une architecture ARM et il est adapté à un usage industriel entre - 40°C et + 85°C.</p>

                <!-- Séparateur -->
                <div class="py-2"></div>

                <!-- Présentation -->
                <p class="lead text-center">Fonctionnant sous Linux, il peut être utilisé comme un ordinateur de bureau ou comme un système embarqué dans des conditions extrêmes.</p>

                <!-- Séparateur -->
                <div class="py-2"></div>

                <!-- Présentation -->
                <p class="lead text-center">Enfin, vous pouvez en faire un serveur auto-hébergé avec les systèmes d'exploitation FreedomBox et YunoHost.</p>

                <!-- Séparateur -->
                <div class="py-4"></div>

                <!-- Titre -->
                <h2 class="font-weight-bold text-center">Bonne utilisation !</h2>

                 <!-- Séparateur -->
                <div class="py-4"></div>

                <!-- site -->
                <div class="btn-toolbar justify-content-center" role="toolbar" aria-label="Groupe de liens">
                  <div class="btn-group mr-2" role="group" aria-label="Site de OLinuXino">
                    <a href="https://www.olimex.com/Products/OLinuXino/" target="_blank" role="button" class="btn">Site officiel <i class="fa fa-external-link" aria-hidden="true"></i></a>
                  </div>
                  <div class="btn-group mr-2" role="group" aria-label="GitHub de OLinuXino">
                    <a href="https://github.com/OLIMEX/OLINUXINO" target="_blank" role="button" class="btn">GitHub <i class="fa fa-external-link" aria-hidden="true"></i></a>
                  </div>
                  <div class="btn-group mr-2" role="group" aria-label="Site de FreedomBox">
                    <a href="https://www.freedombox.org/fr/" target="_blank" role="button" class="btn">FreedomBox <i class="fa fa-external-link" aria-hidden="true"></i></a>
                  </div>
                  <div class="btn-group" role="group" aria-label="Site de YunoHost">
                    <a href="https://yunohost.org/#/index_fr" target="_blank" role="button" class="btn">YunoHost <i class="fa fa-external-link" aria-hidden="true"></i></a>
                  </div>
                </div>

                <!-- Séparateur -->
                <div class="py-2"></div>
            </div>
        </div>
    </div>

    <!-- Séparateur -->
    <div class="py-2"></div>

    <!-- Footer -->
    <?php
      require_once '../require/footer.php';
    ?>

  </body>
</html>
