<!doctype html>
<html lang="fr">
  <head>

   <!-- Title -->
   <title>Protège-moi sur Internet - Éducations</title>
   <link rel="canonical" href="https://protege-moi-sur-internet.com/educations.php">

   <!-- Meta -->
   <meta name="author" content="Yann Jaulin">
   <meta name="keywords" content="Éducations, Internet Archive, Librairies Indépendantes, LoL, une affaire sérieuse, Paf LeGeek, Wikipédia">
   <meta name="description" content="Bienvenue sur la page éducations, proposée par Protège-moi sur Internet !">

   <?php
      require_once 'require/lien.php';
   ?>
  
  </head>
  <body>

  <!-- Navigation -->
  <?php
    require_once 'require/nav.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Alert -->
  <?php
    require_once 'require/alert.php';
  ?>

  <!--Séparateur-->
  <div class="py-4"></div>

  <!-- Fil d'ariane -->
  <div class="container">
    <div class="row">
        <div class="col-md">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="https://protege-moi-sur-internet.com/">Accueil</a></li>
              <li class="breadcrumb-item" aria-current="page">Éducations</li>
            </ol>
          </nav>
        </div>
    </div>
  </div>

  <!--Séparateur-->
  <div class="py-4"></div>

    <!--Catégorie-->
    <div class="container">
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/internetarchive.jpg" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Internet Archive</h5>
                  <p class="card-text">Une bibliothèque numérique gratuite et universelle consacrée à l'archivage culturel !</p>
                  <a href="https://archive.org/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/librairiesindépendantes.png" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Librairies Indépendantes</h5>
                  <p class="card-text">Un moteur de recherche de livres qui fédère plus de 1200 librairies implantées en France !</p>
                  <a href="https://www.librairiesindependantes.com/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
                <div class="card h-100">
                  <img src="https://protege-moi-sur-internet.com/images/loluneaffaireserieuse.png" class="card-img-top" alt="">
                  <div class="card-body">
                    <h5 class="card-title">LoL, une affaire sérieuse</h5>
                    <p class="card-text">Un documentaire soutenu par la Ligue des droits de l'Homme qui met en avant l'importance des logiciels libres et l’impact de l’informatique sur notre vie privée !</p>
                    <a href="https://www.imagotv.fr/documentaires/lol-logiciel-libre-une-affaire-serieuse" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
              <div class="card h-100">
                <img src="https://protege-moi-sur-internet.com/images/paflegeek.png" class="card-img-top" alt="">
                <div class="card-body">
                  <h5 class="card-title">Paf LeGeek</h5>
                  <p class="card-text">Une chaîne dédiée à la présentation d'applications pour protéger votre vie privée sur Internet !</p>
                  <a href="https://www.youtube.com/channel/UCCSHWqosFfYJY5v2WqbTLhg" target="_blank" role="button" class="btn">Se rendre sur la chaîne <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="col mb-4">
                <div class="card h-100">
                  <img src="https://protege-moi-sur-internet.com/images/wikipedia.jpg" class="card-img-top" alt="">
                  <div class="card-body">
                    <h5 class="card-title">Wikipédia</h5>
                    <p class="card-text">Une encyclopédie collaborative, libre, multilingue et universelle gérée en wiki !</p>
                    <a href="https://fr.wikipedia.org/" target="_blank" role="button" class="btn">Se rendre sur le site <i class="fa fa-external-link" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
        </div>
    </div>

    <!-- Séparateur -->
    <div class="py-2"></div>

    <!-- Footer -->
    <?php
      require_once 'require/footer.php';
    ?>

  </body>
</html>