<footer class="text-center">
    <p>
        <i class="fa fa-creative-commons" aria-hidden="true"></i> 
        Créé par <a href="https://yannjaulin.fr/" target="_blank">Yann Jaulin</a> 
        sous la licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank">CC BY-SA 4.0</a>
    </p>
    <ul class="list-inline">
        <li class="list-inline-item"><a href="https://codeberg.org/yann10/protege-moi-sur-internet" target="_blank"><i class="fa fa-gitea" aria-hidden="true"></i> Codeberg</a></li>
        <li class="list-inline-item"><a href="mailto:pmsinternet@tutanota.com" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i> Contact</a></li>
        <li class="list-inline-item"><a href="https://protege-moi-sur-internet.com/sitemap.xml" target="_blank"><i class="fa fa-sitemap" aria-hidden="true"></i> Plan du site</a></li>
    </ul>
</footer>

